`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KU Leuven
// Engineer: Sharmine Catherine Reyes
// 
// Create Date: 30.10.2021 17:47:56
// Design Name: 
// Module Name: VGA_timings_TB
// Project Name: Ass1
// Target Devices: 
// Tool Versions: 
// Description: Testbench of VGA timings
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_timings_TB;

    // signal declaration
    reg        r_iClk;
    reg        r_iRst;
    wire [9:0] w_oCountH;
    wire [9:0] w_oCountV;
    wire       w_oHS;
    wire       w_oVS;
    
    // local parameter declaration
    localparam WIDTH_inst  = 15;
    localparam H_FP_inst   = 2;
    localparam H_PW_inst   = 1;
    localparam H_BP_inst   = 2;    //H_TOT = 20
    localparam HEIGHT_inst = 10;
    localparam V_FP_inst   = 2;
    localparam V_PW_inst   = 1;
    localparam V_BP_inst   = 2;    //V_TOT = 20 

    // instantiation of module under test
    VGA_Timings# (
        .WIDTH      (WIDTH_inst),
        .H_FP       (H_FP_inst),
        .H_PW       (H_PW_inst),
        .H_BP       (H_BP_inst),
        .HEIGHT     (HEIGHT_inst),
        .V_FP       (V_FP_inst),
        .V_PW       (V_PW_inst),
        .V_BP       (V_BP_inst))
    VGA_Timigs_inst (
        .iClk       (r_iClk),
        .iRst       (r_iRst),
        .oHS        (w_oHS),
        .oVS        (w_oVS),
        .oCountH    (w_oCountH),
        .oCountV    (w_oCountV));

    // definition of clock period ------------------------------------------------
    localparam T = 20;
    
    // generation of clock signal ------------------------------------------------
    always begin
        r_iClk = 1;
        #(T/2);
        r_iClk = 0;
        #(T/2);
    end

    // behavioral model ----------------------------------------------------------
    
    reg  [9:0] r_CountH_inst;
    reg  [9:0] r_CountV_inst;    
    wire [9:0] w_CountH_inst;
    wire [9:0] w_CountV_inst;
    wire [9:0] w_oCountH_inst;
    wire [9:0] w_oCountV_inst;
    wire [9:0] w_h_bp;  
    wire [9:0] w_h_fp;
    wire [9:0] w_v_bp;
    wire [9:0] w_v_fp;    
    wire       w_oHS_inst;
    wire       w_oVS_inst;
    wire       w_CountH_max;  // max indicator of r_CountH
    wire       w_CountV_max;  // max indicator of r_CountV
                
    // horizontal counter     
    always @ (posedge r_iClk) begin
        if (r_iRst) begin
            r_CountH_inst <= 0;
        end
        else begin
            r_CountH_inst <= w_CountH_inst;
        end
    end
 
    // draw + FPh + SYNCh + BPh
    assign w_h_bp = WIDTH_inst + H_FP_inst + H_PW_inst + H_BP_inst;
    // draw + FPh    
    assign w_h_fp = WIDTH_inst + H_FP_inst;
    // draw + FPv + SYNCv + BPv    
    assign w_v_bp = HEIGHT_inst + V_FP_inst + V_PW_inst + V_BP_inst;
    // draw + FPv
    assign w_v_fp = HEIGHT_inst + V_FP_inst;      
          
    // hcrizontal counter
    assign w_CountH_inst = (r_CountH_inst == w_h_bp - 1) ? 0: r_CountH_inst + 1;    

    // output horizontal counter
    assign w_oCountH_inst = r_CountH_inst;

    // output horizontal flag
    assign w_oHS_inst = (r_CountH_inst == w_h_fp)? 0: 1; 

    // vertical counter
    assign w_CountH_max  = (r_CountH_inst == w_h_bp - 1) ? 1: 0;  // indicator when r_CountH reached max count 
    assign w_CountV_max  = (r_CountV_inst == w_v_bp - 1) ? 1: 0;  // indicator when r_CountV reached max counts
    assign w_CountV_inst =  r_CountV_inst + 1;                    // increment r_CountV
           
    // vertical counter 
    always @ (posedge r_iClk) begin
        if (r_iRst) begin
            r_CountV_inst <= 0;
        end
        else if (w_CountV_max && w_CountH_max) begin // reset r_CountV_inst when r_CountV_inst and r_CountH reached max count
            r_CountV_inst <= 0;
        end
        else if (w_CountH_max) begin // increment r_CountV_inst when r_CountH reached max count
            r_CountV_inst <= w_CountV_inst;
        end
        else begin                    // retain value
            r_CountV_inst <= r_CountV_inst;  
        end   
    end  
    
     // output vertical flag
    assign w_oVS_inst = (r_CountV_inst == w_v_fp)? 0: 1; 
          
    // output vertical counter
    assign w_oCountV_inst = r_CountV_inst;
    
    // test vector for oCountH            
    always @ (negedge r_iClk) begin
         if (w_oCountH_inst == w_oCountH) $display ("%3d ns: Expected oCountH = %0d; Actual oCountH = %0d : PASSED!!! ", $time, w_oCountH_inst, w_oCountH);
         else                             $display ("%3d ns: Expected oCountH = %0d; Actual oCountH = %0d : FAILED!!! ", $time, w_oCountH_inst, w_oCountH);            
    end
 
    // test vector for oCountV            
    always @ (negedge r_iClk) begin
         if (w_oCountV_inst == w_oCountV) $display ("%3d ns: Expected oCountV = %0d; Actual oCountV = %0d : PASSED!!! ", $time, w_oCountV_inst, w_oCountV);
         else                             $display ("%3d ns: Expected oCountV = %0d; Actual oCountV = %0d : FAILED!!! ", $time, w_oCountV_inst, w_oCountV);            
    end
       
    // test vector for oHS     
    always @ (negedge r_iClk) begin
         if (w_oHS_inst == w_oHS)        $display ("%3d ns: Expected oHS     = %0d; Actual oHS     = %0d : PASSED!!! ", $time, w_oHS_inst, w_oHS);
         else                            $display ("%3d ns: Expected oHS     = %0d; Actual oHS     = %0d : FAILED!!! ", $time, w_oHS_inst, w_oHS);            
    end    
    
    // test vector for oVS     
    always @ (negedge r_iClk) begin
         if (w_oVS_inst == w_oVS)        $display ("%3d ns: Expected oVS     = %0d; Actual oVS     = %0d : PASSED!!! ", $time, w_oVS_inst, w_oVS);
         else                            $display ("%3d ns: Expected oVS     = %0d; Actual oVS     = %0d : FAILED!!! ", $time, w_oVS_inst, w_oVS);            
    end   
        
    // stimulus generator --------------------------------------------------------
    initial begin
        r_iRst = 1;
        #50;
        r_iRst = 0;
        
        #(100*T);
        //$stop;
    end
endmodule
