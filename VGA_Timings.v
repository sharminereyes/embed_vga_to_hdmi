`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KU Leuven
// Engineer: Sharmine Catherine Reyes
//           Zeyu Guan
// 
// Create Date: 30.10.2021 15:06:58
// Design Name: 
// Module Name: VGA_timings
// Project Name: Ass1
// Target Devices: 
// Tool Versions: 
// Description: Design and simulation of the VGA timings
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Revision 0.02 - modify w_CmpV (November 12, 2021)
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_Timings#(
    // H total = 640 + 16 + 96 + 48 = 800
    parameter WIDTH  = 640,
    parameter H_FP   = 16,
    parameter H_PW   = 96,
    parameter H_BP   = 48,
    // H total = 480 + 10 + 2 + 33 = 525 
    parameter HEIGHT = 480,
    parameter V_FP   = 10,
    parameter V_PW   = 2,
    parameter V_BP   = 33
    )
    (
    input  wire       iClk,
    input  wire       iRst,
    output wire       oHS,
    output wire       oVS,
    // 10 bits are sufficient. Count up to 2^10 - 1 = 1023
    output wire [9:0] oCountH,
    output wire [9:0] oCountV,
    output wire       oActive
    );
 
    reg  [9:0] r_CountH_Curr;
    reg  [9:0] r_CountV_Curr;
    wire [9:0] w_CountH_Next;
    wire [9:0] w_CountV_Next;
    wire       w_RstCntH;
    wire       w_RstCntV;
    wire       w_CmpV; 
    wire       w_CmpH;


    // full horizontal timing 0-799
    assign w_CmpH        = (r_CountH_Curr == (WIDTH+H_FP+H_PW+H_BP-1));
    // full vertical timing  0-524
    assign w_CmpV        = (r_CountV_Curr == (HEIGHT+V_FP+V_PW+V_BP));
        
    // horizontal increment
    assign w_CountH_Next = r_CountH_Curr + 1;
    // vertical increment
    assign w_CountV_Next = r_CountV_Curr + 1;
        
    // reset of horizontal counter
    assign w_RstCntH     = iRst || w_CmpH;
    // reset of vertical counter
    assign w_RstCntV     = (iRst) || (w_CmpV && w_CmpH);
        
    // horizontal counter
    always @ (posedge iClk) begin
        if (w_RstCntH) begin
            r_CountH_Curr <= 0;
        end
        else begin
            r_CountH_Curr <= w_CountH_Next;
        end
    end

    // Vertical Counter
    always @ (posedge iClk) begin
        if (w_RstCntV) begin
            r_CountV_Curr = 0;
        end
        else if (w_CmpH) begin
            r_CountV_Curr = w_CountV_Next;
        end
    end
    

    // Horizontal output counter
    assign oCountH = r_CountH_Curr;
    // Vertical output counter
    assign oCountV = r_CountV_Curr;
 
    // Output Low SYNCh
    assign oHS     = ( (r_CountH_Curr>=(WIDTH +H_FP)) && (r_CountH_Curr<(WIDTH +H_FP+H_PW)) ) ? 0: 1;
    // Output Low SYNCv
    assign oVS     = ( (r_CountV_Curr>=(HEIGHT+V_FP)) && (r_CountV_Curr<(HEIGHT+V_FP+V_PW)) )? 0: 1;
    
    //assign oActive = 1'b1;
    assign oActive = ((r_CountH_Curr<(WIDTH)) && (r_CountV_Curr<(HEIGHT))) ? 1: 0;
    
 endmodule
 