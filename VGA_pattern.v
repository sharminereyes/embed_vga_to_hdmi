`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KU Leuven
// Engineer: Sharmine Catherine Reyes
//           Zeyu Guan 
//
// Create Date: 30.10.2021 23:33:55
// Design Name: 
// Module Name: VGA_pattern
// Project Name: Ass1
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Revision 0.02 - Modify All Red Pattern (November 9, 2021)
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGA_pattern #(
    // H total = 640 + 16 + 96 + 48 = 800
    parameter WIDTH = 640,
    parameter H_FP  = 16,
    parameter H_PW  = 96,
    parameter H_BP  = 48,
    // V total = 480 + 10 + 2 + 33 = 525
    parameter HEIGHT = 480,
    parameter V_FP   = 10,
    parameter V_PW   = 2,
    parameter V_BP   = 33
    )
    (
    input  wire       iClk,
    input  wire       iRst,
    input  wire [9:0] iCountH,
    input  wire [9:0] iCountV,
    input  wire       iHs,
    input  wire       iVs,
    input  wire       iActive,
    input  wire [3:0] iPattern,
    output wire       oHs,
    output wire       oVs,
    output wire       oActive,
    // 12 bits RGB
    output wire [3:0] oRed,
    output wire [3:0] oGreen,
    output wire [3:0] oBlue
    );

    reg [9:0] r_iCountH;
    reg [9:0] r_iCountV;
 
    wire wActive;


    // Modify code if pattern 1 or 2 --------------------------------------------------------          
    // pattern1: All Red
    
    
    //assign wActive = ((iCountH<(WIDTH)) && (iCountV<(HEIGHT))) ? 1: 0;
    
    
    assign oRed     = (iActive==1) ? 4'b1111: 4'b0000;
    //assign oRed = (iActive==1) ?iPattern: 4'b0000;
    assign oGreen   = 4'b0000;
    assign oBlue    = 4'b0000;
    
    assign oActive = iActive;
    
   
    
    //pattern2: vertical color bars
    // Divides active area into 8 Equal Bars and colors them accordingly
    // Colors Each According to this Truth Table:
    // R G B  w_Bar_Sel  Ouput Color
    // 0 0 0       0        Black
    // 0 0 1       1        Blue
    // 0 1 0       2        Green
    // 0 1 1       3        Turquoise
    // 1 0 0       4        Red
    // 1 0 1       5        Purple
    // 1 1 0       6        Yellow
    // 1 1 1       7        White        

/*
    wire [7:0] w_active_bar;
    assign w_active_bar[0] = (iCountH < 80) ? 1:0 ;
    assign w_active_bar[1] = (iCountH >= 80  && iCountH <160) ? 1:0;
    assign w_active_bar[2] = (iCountH >= 160 && iCountH <240) ? 1:0; 
    assign w_active_bar[3] = (iCountH >= 240 && iCountH <320) ? 1:0; 
    assign w_active_bar[4] = (iCountH >= 320 && iCountH <400) ? 1:0; 
    assign w_active_bar[5] = (iCountH >= 400 && iCountH <480) ? 1:0; 
    assign w_active_bar[6] = (iCountH >= 480 && iCountH <560) ? 1:0; 
    assign w_active_bar[7] = (iCountH >= 560 && iCountH <640) ? 1:0; 
    
    assign oRed     = (w_active_bar[0] || w_active_bar[1] ||                                       w_active_bar[4] ||  w_active_bar[5]                   ) ? 4'b1111: 4'b0000;
    assign oGreen   = (w_active_bar[0] || w_active_bar[1] || w_active_bar[2] || w_active_bar[3]                                                          ) ? 4'b1111: 4'b0000;
    assign oBlue    = (w_active_bar[0] ||                    w_active_bar[2] ||                    w_active_bar[4]                     ||w_active_bar[6] ) ? 4'b1111: 4'b0000;       
*/    
/*
    wire [9:0] w_BarH;                              //bit size is same as iCountH
    wire [2:0] w_Bar_Sel;                           //bit size is 3 for the count of horizontal bars (8 bars)    
    
    assign w_BarH    = WIDTH/8;
    assign w_Bar_Sel = (iCountH<w_BarH*1) ? 0:      // iCountH < 80
                       (iCountH<w_BarH*2) ? 1:      // iCountH < 160
                       (iCountH<w_BarH*3) ? 2:      // iCountH < 240
                       (iCountH<w_BarH*4) ? 3:      // iCountH < 320      
                       (iCountH<w_BarH*5) ? 4:      // iCountH < 400
                       (iCountH<w_BarH*6) ? 5:      // iCountH < 480
                       (iCountH<w_BarH*7) ? 6: 7;   // iCountH < 560 

    assign oRed     = ((w_Bar_Sel == 0) || 
                       (w_Bar_Sel == 1) ||
                       (w_Bar_Sel == 4) ||
                       (w_Bar_Sel == 5)   )? 4'b1111: 4'b0000; 

    assign oGreen   = ((w_Bar_Sel == 0) || 
                       (w_Bar_Sel == 1) ||
                       (w_Bar_Sel == 2) ||
                       (w_Bar_Sel == 3)   )? 4'b1111: 4'b0000;
                       
    assign oBlue    = ((w_Bar_Sel == 0) || 
                       (w_Bar_Sel == 2) ||
                       (w_Bar_Sel == 4) ||
                       (w_Bar_Sel == 6)   )? 4'b1111: 4'b0000;                                                              
 */   

//    // Fixed output (do not modify) -------------------------------------------------------- 
    assign oHs = iHs;
    assign oVs = iVs;
    
    //assign oActive = iActive;
    
    
    
endmodule

